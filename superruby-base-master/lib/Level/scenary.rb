class Scenary

  def initialize(level)
    @level = level
    @left_ground = Gosu::Image.new("media/images/levels/#{@level}/tiles/left_ground.png")
    @middle_ground = Gosu::Image.new("media/images/levels/#{@level}/tiles/middle_ground.png")
    @right_ground = Gosu::Image.new("media/images/levels/#{@level}/tiles/right_ground.png")
    @extra1 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra1.png")
    @extra2 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra2.png")
    @extra1_y = Utils::FLOOR_Y - @extra1.height * 2
    @extra2_y = Utils::FLOOR_Y - @extra2.height * 2
    @extra1_x = Random.rand(50..300)
    @extra2_x = Random.rand(50..300)
    @extra3 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra3.png")
    @extra4 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra4.png")
    @extra5 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra5.png")
    @extra6 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra6.png")
    @extra7 = Gosu::Image.new("media/images/levels/#{@level}/tiles/extra7.png")
    @extra3_x = Random.rand(1200..1600)
    @extra4_x = Random.rand(1200..1600)
    @extra5_x = Random.rand(1200..1600)
    @extra6_x = Random.rand(1200..1600)
    @extra7_x = Random.rand(1200..1600)
  end

  def draw
    @@last_ground_x = 0
    @left_ground.draw(@@last_ground_x, Utils::FLOOR_Y, 10)
    @@last_ground_x = @@last_ground_x + 128
    while @@last_ground_x < Utils::SCREEN_WIDTH - @right_ground.width
      @middle_ground.draw(@@last_ground_x, Utils::FLOOR_Y, 10)
      @@last_ground_x = @@last_ground_x + 128
    end
    @right_ground.draw(@@last_ground_x, Utils::FLOOR_Y, 10)
    @extra1.draw(@extra1_x, @extra1_y, 2, 2, 2)
    @extra2.draw(@extra2_x, @extra2_y, 2, 2, 2)
    @extra3.draw(@extra3_x, Utils::FLOOR_Y - @extra3.height, 2)
    @extra4.draw(@extra4_x, Utils::FLOOR_Y - @extra4.height, 2)
    @extra5.draw(@extra5_x, Utils::FLOOR_Y - @extra5.height, 2)
    @extra6.draw(@extra6_x, Utils::FLOOR_Y - @extra6.height, 2)
    @extra7.draw(@extra7_x, Utils::FLOOR_Y - @extra7.height, 2)
  end

  def update
  end
end
