class LifeCounter
MARGIN_INTERNAL_X = 4
MARGIN_TOP = 10
MARGIN_LEFT = 10
attr_accessor :lives
  def initialize
    @lives = 3
    @image = Gosu::Image.new('media/images/heart.png')
    @get_life_sound = Gosu::Sample.new('media/sounds/effects/life_collected.ogg')
    @width = @image.width * 2
  end

  def draw
    @lives.times do |posicion|
      x = MARGIN_LEFT + posicion * (@width + MARGIN_INTERNAL_X)
      @image.draw(x, MARGIN_TOP, 2, 2, 2)
    end
  end

  def update
  end

  def lose_life!
    @lives -= 1
  end

  def get_life!(music)
    if @lives < 6
      @lives += 1
      @get_life_sound.play if music == true
    end
  end

  def game_over?
    if @lives == 0
      return true
    else
      return false
    end
  end
end
