require_relative 'background'
require_relative 'life_counter'
require_relative 'score'
require_relative 'hero'
require_relative 'item_life'
require_relative 'enemy'
require_relative 'collectable'
require_relative 'items'
require_relative 'scenary'
class Level
  attr_accessor :actual_time
  def initialize(game, enemy_definitions, level, collect_definitions, points, lives)
    @game = game
    @actual_level = level
    @lives = lives
    @points = points
    @enemy_definitions = enemy_definitions
    @collec_definitions = collect_definitions
    @backgrounds_path = {
      snow: "snow",
      grass: "grass",
      desert: "desert"
    }
    @game.actual_music = Gosu::Song.new("media/sounds/music/level_#{level}.ogg")
    @game.actual_music.play if @game.song == true
    if @actual_level == 1
      @background = Background.new(:snow)
    elsif @actual_level == 2
      @background = Background.new(:desert)
    elsif @actual_level == 3
      @background = Background.new(:grass)
    end
    @life_counter = LifeCounter.new
    @score = Score.new
    @scenary = Scenary.new(@actual_level)
    @hero = Hero.new(@game, @game.hero_run, @game.hero_jump, @game.hero_dead, @game.hero_idle, @game.current_hero_run, @game.current_hero_jump, @game.current_hero_dead, @game.current_hero_idle)
    @items_lifes = []
    @enemies = []
    @collectables = []
    @items = Items.new(@actual_level)
    @random_sound = Gosu::Sample.new('media/sounds/effects/eagle.ogg')
    @score.points = @points
    @life_counter.lives = @lives
  end

  def draw
    @scenary.draw
    @background.draw
    @life_counter.draw
    @items.draw
    @score.draw
    @hero.draw
    unless @items_lifes.empty?
      @items_lifes.each do |life|
        life.draw
      end
    end
    unless @enemies.empty?
      @enemies.each do |enemy|
        enemy.draw
      end
    end
    unless @collectables.empty?
      @collectables.each do |collectable|
        collectable.draw
      end
    end
  end

  def update
    @actual_time = Time.now
    @items.update
    @hero.update
    if @level == 1
      @random_sound.play if Random.rand(1..100) == 50 && @game.song == true
    end
    #Coleccionables
    create_collec
    unless @collectables.empty?
      @collectables.each do |collectable|
      collectable.fall! if collectable.y < Utils::FLOOR_Y - collectable.height
      if @hero.needs_respawn == false
        if @hero.bumped_into?(collectable)
          @score.update_score!(collectable.points)
          collectable.collect(@game.song)
          @items.update_score!
        end
      end
      if Utils.compare_time?(@actual_time, collectable.start_time, 10)
        collectable.expire
      end
    end
    @collectables.reject! { |collectable| collectable.collected? or collectable.expired? }
  end
    #_______________________________________________________________________________________
    #Enemigos
    if @actual_level == 1
      create_enemy if @enemies.length < 5
    elsif @actual_level == 2
      create_enemy if @enemies.length < 8
    elsif @actual_level == 3
      create_enemy if @enemies.length < 10
    end
    unless @enemies.empty?
      @enemies.each do |enemy|
        enemy.update
        if enemy.y < Utils::FLOOR_Y - enemy.height
          enemy.fall!
        else
          if enemy.status == :walk && enemy.side == 1
            if enemy.x > 100
              enemy.walk_left!
            else
              enemy.side = 2
            end
          elsif enemy.status == :walk && enemy.side == 2
            if enemy.x + enemy.width < Utils::SCREEN_WIDTH
              enemy.walk_right!(@game.width)
            else
              enemy.side = 1
            end
          elsif enemy.status == :run && enemy.side == 1
            if enemy.x > 100
              enemy.run_left!
            else
              enemy.status = :idle
            end
          elsif enemy.status == :run && enemy.side == 2
            if enemy.x - enemy.height > Utils::SCREEN_WIDTH
              enemy.run_right!(@game.width)
            else
              enemy.side = 1
            end
          elsif enemy.status == :jump
              enemy.jump!
          end
          @offset = @hero.y + @hero.height - enemy.y
          if (@offset == 0) && (@hero.bumped_into?(enemy)) && @hero.status != :dead && enemy.status != :dead
            enemy.die!(@game.song)
            @hero.jump!
          end
          if @hero.bumped_into?(enemy) && (@hero.needs_respawn == false && enemy.status != :dead)
            @hero.die!(@game.song)
            @life_counter.lose_life!
          end
        end
      end
      @enemies.reject! { |enemy| enemy.dead?(@actual_time) }
    end
    #______________________________________________________________________
    #Vidas
    @items_lifes << ItemLife.new if Utils.is_possible?(10000, 30)
    unless @items_lifes.empty?
      @items_lifes.each do |life|
        life.fall! if life.y <  Utils::FLOOR_Y - life.height
        if @hero.needs_respawn == false
          if @hero.bumped_into?(life)
            @life_counter.get_life!(@game.song)
            @score.update_score!(500)
            life.collect(@game.song)
          end
        end
        if Utils.compare_time?(@actual_time, life.start_time, 10)
          life.expire
        end
      end
      @items_lifes.reject! { |life| life.collected? or life.expired? }
    end
    #_____________________________________________________________________________
    #Hero
    if @hero.status != :dead
      if @game.button_down?(Gosu::KbA) or @game.button_down?(Gosu::KbLeft)
         if @hero.x > 0 + @hero.width
           @hero.move_left!
           unless @hero.status == :jump || @hero.status == :dead
             @hero.status = :run
            end
         else
           unless @hero.status == :jump || @hero.status == :dead
             @hero.status = :idle
           end
         end
      elsif @game.button_down?(Gosu::KbD) or @game.button_down?(Gosu::KbRight)
         if @hero.x < Utils::SCREEN_WIDTH - @hero.width
           @hero.move_right!(@game.width)
           unless @hero.status == :jump || @hero.status == :dead
            @hero.status = :run
           end
         elsif @hero.status != :dead
           unless @hero.status == :jump || @hero.status == :dead
             @hero.status = :idle
           end
         end
       elsif (@game.button_down?(Gosu::KbW) || @game.button_down?(Gosu::KbUp)) && @hero.status != :jump
           @hero.jump!
       end

       if (@game.button_down?(Gosu::KbA) || @game.button_down?(Gosu::KbLeft)) && (@game.button_down?(Gosu::KbW) || @game.button_down?(Gosu::KbUp)) && @hero.status != :jump
         if @hero.x > 0 + @hero.width
           @hero.move_left!
         end
         @hero.jump!
       elsif (@game.button_down?(Gosu::KbD) || @game.button_down?(Gosu::KbRight)) && (@game.button_down?(Gosu::KbW) || @game.button_down?(Gosu::KbUp)) && @hero.status != :jump
         if @hero.x < Utils::SCREEN_WIDTH - @hero.width
           @hero.move_right!(@game.width)
         end
         @hero.jump!
       end
       unless (@game.button_down?(Gosu::KbD) || @game.button_down?(Gosu::KbRight)) || (@game.button_down?(Gosu::KbA) || @game.button_down?(Gosu::KbLeft)) || (@game.button_down?(Gosu::KbW) || @game.button_down?(Gosu::KbUp)) || (@game.button_down?(Gosu::KbW) || @game.button_down?(Gosu::KbUp)) || @hero.status == :dead || @hero.status == :jump
         @hero.status = :idle
       end
     end
     if @hero.status != :jump
       @hero.fall! unless @hero.y + @hero.width <= Utils::FLOOR_Y
     end
    #________________________________________________________________________________________
    @game.show_game_over!(@score.points) if @life_counter.game_over?
    if @actual_level == 1
      if @items.items_c == @items.items_n
        @game.level = 2
        @game.level_title!(@life_counter.lives, @score.points)
      end
    elsif @actual_level == 2
      if @items.items_c == @items.items_n
        @game.level = 3
        @game.level_title!(@life_counter.lives, @score.points)
      end
    elsif @actual_level == 3
      @game.show_game_over!(@score.points) if @items.items_c == @items.items_n
    end
  end

  def button_down(id)
    case id
    when Gosu::KbEscape
      @game.show_confirmation!(@background)
    end
  end

  def create_enemy
    info = @enemy_definitions.sample
    if @actual_level == 1
      if Utils.is_possible?(10000, info[:posibility])
        @enemies << Enemy.new(info[:image_path], info[:points], @window, @game.enemy_run, @game.enemy_jump, @game.enemy_dead, @game.enemy_idle, @game.enemy_walk, @game.current_enemy_run, @game.current_enemy_jump, @game.current_enemy_dead, @game.current_enemy_idle, @game.current_enemy_walk)
      end
    elsif @actual_level == 2
      if Utils.is_possible?(10000, info[:posibility])
        @enemies << Enemy.new(info[:image_path], info[:points], @window, @game.enemy_run, @game.enemy_jump, @game.enemy_dead, @game.enemy_idle, @game.enemy_walk, @game.current_enemy_run, @game.current_enemy_jump, @game.current_enemy_dead, @game.current_enemy_idle, @game.current_enemy_walk)
      end
    elsif @actual_level == 3
      if Utils.is_possible?(10000, info[:posibility])
        @enemies << Enemy.new(info[:image_path], info[:points], @window, @game.enemy_run, @game.enemy_jump, @game.enemy_dead, @game.enemy_idle, @game.enemy_walk, @game.current_enemy_run, @game.current_enemy_jump, @game.current_enemy_dead, @game.current_enemy_idle, @game.current_enemy_walk)
      end
    end
  end

  def create_collec
    info = @collec_definitions.sample
    if  Utils.is_possible?(10000, info[:posibility])
      @collectables << Collectable.new(info[:image_path], info[:points], @window, @actual_level)
    end
  end
end
