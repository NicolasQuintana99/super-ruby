class GameOver

  def initialize(game, points, music)
    @game = game
    @text = Gosu::Image.from_text('Game Over', Utils::FONT_SIZE_BIG, font: Utils.default_font)
    @points = Gosu::Image.from_text("Puntos: #{points}", Utils::FONT_SIZE_BIG, font: Utils.default_font)
    @text_x = Utils.center_x(@text)
    @text_y = Utils.center_y(@text)
    @points_x = Utils.center_x(@points)
    @points_y = Utils.center_y(@points)
    @end_game_sound = Gosu::Sample.new('media/sounds/effects/life_collected.ogg')
    @end_game_sound.play if music == true
  end

  def update

  end

  def draw
    @text.draw(@text_x, @text_y, 9)
    @points.draw(@points_x, @points_y + 100, 9)
  end

  def button_down(id)
    if id == Gosu::KbEscape
      @game.show_main_menu!
    end
  end
end
