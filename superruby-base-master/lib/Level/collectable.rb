class Collectable
attr_accessor :x, :y, :width, :height, :status, :image_path, :points, :start_time
SCALE = 6
  def initialize(image_path, points, game, level)
    @game = game
    @image_path = image_path
    @points = points
    @level = level
    if @level == 1
      @image = Gosu::Image.new("media/images/levels/#{@level}/items/snowman.png")
    elsif @level == 2
      @image = Gosu::Image.new("media/images/levels/#{@level}/items/skeleton.png")
    elsif @level ==
      @image = Gosu::Image.new("media/images/levels/#{@level}/items/mushroom_1.png")
    end
    @width = @image.width
    @height = @image.height
    @x = Random.rand(1024 - @width)
    @y = 0 - @height
    @dead = false
    @state = {
      collected: "collected",
      expired: "expired"
    }
    @start_time = Time.new
    @start_time = @start_time.to_i
    @get_collectable_sound = Gosu::Sample.new('media/sounds/effects/item_collected.ogg')
  end

  def draw
    @image.draw(@x, @y, 3)
  end

  def fall!
    @y += 8
  end

  def expire
    @status = :expired
  end

  def expired?
    if @status == :expired
      return true
    else
      return false
    end
  end

  def collect(music)
    @get_collectable_sound.play if music == true
    @status = :collected
  end

  def collected?
    if @status == :collected
      return true
    else
      return false
    end
  end
end
