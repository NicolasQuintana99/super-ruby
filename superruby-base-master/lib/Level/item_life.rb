class ItemLife
  attr_accessor :x, :y, :height, :width, :status, :start_time
  def initialize
    @image = Gosu::Image.new('media/images/heart.png')
    @height = @image.height
    @width = @image.width
    @x = Random.rand(1024)
    @y = 0 - @image.height
    @collected = false
    @state = {
      collected: "collected",
      expired: "expired"
    }
    @start_time = Time.now
    @start_time = @start_time.to_i
    @collect_life_sound = Gosu::Sample.new('media/sounds/effects/life_new.ogg')
  end

  def draw
    @image.draw(@x, @y, 3)
  end

  def fall!
    @y += 8
  end

  def expire
    @status = :expired
  end

  def expired?
    if @status == :expired
      return true
    else
      return false
    end
  end

  def collect(music)
    @collect_life_sound.play if music == true
    @status = :collected
  end

  def collected?
    if @status == :collected
      return true
    else
      return false
    end
  end
end
