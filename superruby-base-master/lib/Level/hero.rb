class Hero
  attr_accessor :x, :y, :height, :width, :actual_height, :status, :needs_respawn

    def initialize(game, run, jump, dead, idle, current_run, current_jump, current_dead, current_idle)
      @last_position = 'right'
      @game = game
      @animations = {
        idle: "idle",
        run: "run",
        jump: "jump",
        dead: "dead"
      }
      @status = :idle
      @timmer = 0
      @scale_x = 1
      @scale_y = 1
      @run = run
      @jump = jump
      @dead = dead
      @idle = idle
      @current_run = current_run
      @current_jump = current_jump
      @current_dead = current_dead
      @current_idle = current_idle
      @width = @idle.first.width
      @height = @idle.first.height
      @x = 0
      @y = Utils::FLOOR_Y - @height
      @state = 0xffffffff
      @needs_respawn = false
      @die_sound = Gosu::Sample.new('media/sounds/effects/player_dead.ogg')
    end

  def draw
    if @status == :run
      hero = @run[@current_run  % 10] #Controla que el indice de la animacion no sea superior a 9
    elsif @status == :idle
      hero = @idle[@current_idle % 10]
    elsif @status == :jump
      hero = @jump[@current_jump  % 10]
    elsif @status == :dead
      hero = @dead[@current_dead  % 10]
    end
    hero.draw(@x, @y, 4, @scale_x, @scale_y, @state)
  end

  def update
    @actual_time = Time.now
    if @status == :run
      @current_run += 0.2
    elsif @status == :idle
      @current_idle += 0.2
    elsif @status == :dead
      @current_dead += 0.1
    elsif @status == :jump
      @current_jump += 0.2
    end
    if @status == :jump
      jumping!
    end
    if @needs_respawn == true
      respawn!(@actual_time)
    end
  end

  def move_left! ## Mover Izquierda
    if @last_position == 'right'
      @x = @x + @width
    end
    if @x > 0 + @width
        @x-= 4
        @scale_x = -1
        @last_position = 'left'
    end
  end

  def move_right!(width) ## Mover Derecha
    if @last_position == 'left'
      @x = @x - @width
    end
    if @x < width - @width
        @x+= 4
        @scale_x = 1
        @last_position = 'right'
    end
  end

  def jump!
    @jump_speed = 20
    @status = :jump
  end

  def jumping!
    @jump_speed -= 1
    if @jump_speed > 0
      @jump_speed.times do
        @y -= 1
      end
    end
    if @jump_speed < 0
       (-@jump_speed).times do
         if @y < (Utils::FLOOR_Y - @height)
           @y += 1
         end
       end
    end
     if @y >= (Utils::FLOOR_Y - @height)
       @y = Utils::FLOOR_Y - @height
       @status = :idle
     end
  end

  def bumped_into?(object) ## colisiones
    hero_left = @x
    hero_right = @x + @width ## Anotacion : Se divide entre 2 @widht por que la escala es 0.5
    hero_top = @y
    hero_bottom = @y + @height ## Anotacion : Se divide entre 2 @height por que la escala es 0.5
    object_left = object.x
    object_right = object.x + object.width # Anotacion : Se divide entre 2 @widht por que la escala es 0.5
    object_top = object.y
    object_bottom = object.y + object.height## Anotacion : Se divide entre 2 @height por que la escala es 0.5

    if hero_left > object_right
      return false
    elsif hero_right < object_left
      return false
    elsif hero_top > object_bottom
      return false
    elsif hero_bottom < object_top
      return false
    else
      return true
    end
  end

  def die!(music)
    @status = :dead
    @animation_time = Time.now
    @animation_time = @animation_time.to_i
    @needs_respawn = true
    @die_sound.play if music == true
  end

  def respawn!(actual_time)
    if @needs_respawn == true
      if @y < Utils::FLOOR_Y - @height
        fall!
      end
      if Utils.compare_time?(actual_time, @animation_time, 1)
        @status = :idle if @status == :dead
        @state = 0x33ffffff
      end
      if Utils.compare_time?(@actual_time, @animation_time, 3)
        @state = 0xffffffff
        @needs_respawn = false
        @current_dead = 0
      end
    end
  end

  def fall!
    @y += 7
  end
end
