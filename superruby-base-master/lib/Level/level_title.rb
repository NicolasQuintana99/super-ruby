class LevelTitle

  def initialize(game, level)
    @level = level#indica en que lv del juego se esta
    @game = game
    @title = Gosu::Image.from_text("Level #{@level}", Utils::FONT_SIZE_BIG, font: Utils.default_font)
    @x = Utils.center_x(@title)
    @y = Utils.center_y(@title)
  end

  def draw
    @title.draw(@x, @y, 1)
  end

  def update
  end

  def button_down(id)#Si no se coloca este metodo el juego explota al presionar cualquier tecla en la pantalla de carga
  end
end
