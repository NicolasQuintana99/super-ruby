class Background

  def initialize(path)
    @path = path
    @image = Gosu::Image.new("media/images/menu/#{@path}.png")
  end

  def draw
    @image.draw(0,0,1, 2, 2)
  end
end
