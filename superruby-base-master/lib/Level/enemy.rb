class Enemy
  attr_accessor :x, :y, :height, :width, :actual_height, :status, :animation_time, :side, :points

    def initialize(image_path, points, game, run, jump, dead, idle, walk, current_run, current_jump, current_dead, current_idle, current_walk)
      @last_position = 'right'
      @game = game
      @image_path = image_path
      @points = points
      @animations = {
        idle: "idle",
        run: "run",
        jump: "jump",
        dead: "dead",
        walk: "walk"
      }
      @status = :idle
      @x = Random.rand(950)
      @y = 0
      @scale_x = 1
      @scale_y = 1
      @run = run
      @jump = jump
      @dead = dead
      @idle = idle
      @walk = walk
      @current_run = current_run
      @current_jump = current_jump
      @current_dead = current_dead
      @current_idle = current_idle
      @current_walk = current_walk
      @width = @idle.first.width
      @height = @idle.first.height
      @action_time = Random.rand(3000..5000)
      @change_action = 0
      @animation_time = 0
      @die_sound = Gosu::Sample.new('media/sounds/effects/monster_killed.ogg')
    end

  def draw
    if @status == :run
      enemy = @run[@current_run  % 10] #Controla que el indice de la animacion no sea superior a 9
    elsif @status == :idle
      enemy = @idle[@current_idle % 10]
    elsif @status == :jump
      enemy = @jump[@current_jump  % 10]
    elsif @status == :dead
      enemy = @dead[@current_dead  % 10]
    elsif @status == :walk
      enemy = @walk[@current_walk % 10]
    end
    enemy.draw(@x, @y, 3, @scale_x, @scale_y)
  end

  def update
    if @status == :run
      @current_run += 0.2
    elsif @status == :idle
      @current_idle += 0.2
    elsif @status == :dead
      @current_dead += 0.1
    elsif @status == :jump
      @current_jump += 0.2
    elsif @status == :walk
      @current_walk += 0.2
    end
    select_action
    if @jumping == true
      jumping!
    end
  end

  def run_left! ## Mover Izquierda
    if @last_position == 'right'
      @x = @x + @width
    end
    if @x > 0 + @width
        @x-= 8
        @scale_x = -1
        @last_position = 'left'
    end
  end

  def run_right!(width) ## Mover Derecha
    if @last_position == 'left'
      @x = @x - @width
    end
    if @x < width - @width
        @x+= 8
        @scale_x = 1
        @last_position = 'right'
    end
  end

  def walk_left! ## Mover Izquierda
    if @last_position == 'right'
      @x = @x + @width
    end
    if @x > 0 + @width
        @x-= 4
        @scale_x = -1
        @last_position = 'left'
    end
  end

  def walk_right!(width) ## Mover Derecha
    if @last_position == 'left'
      @x = @x - @width
    end
    if @x < width - @width
        @x+= 4
        @scale_x = 1
        @last_position = 'right'
    end
  end

  def select_action
    @current_time = (Gosu::milliseconds/@action_time)%2
    if @status != :dead
      if @change_action != @current_time
        @action = Random.rand(1..4)
        case @action
        when 1
          @status = :idle
        when 2
          @status = :jump
        when 3
          @status = :walk
          @side = Random.rand(1..2)
        when 4
          @status = :run
          @side = Random.rand(1..2)
        end
        @change_action = @current_time
      end
    end
  end

  def jump!
    @vs = 20
    @jumping = true
  end

  def jumping!
    @vs -= 1
    if @vs > 0
      @vs.times do
        @y -= 1
      end
    end
    if @vs < 0
       (-@vs).times do
         if @y < (Utils::FLOOR_Y - @height)
           @y += 1
         end
       end
    end
    if @vs == 0
      @status = :idle
    end
  end

  def die!(music)
    @status = :dead
    @animation_time = Time.now
    @animation_time = @animation_time.to_i
    @die_sound.play if music == true
  end

  def dead?(actual_time)
    if @status == :dead && Utils.compare_time?(actual_time, @animation_time, 1)
      return true
    end
  end

  def fall!
    @y += 6
  end
end
