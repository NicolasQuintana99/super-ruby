require 'gosu'
class Items
attr_accessor :items_c, :items_n
Y_POSITION = 10
  def initialize(level)
    @level = level
    if @level == 1
      @items_n = 10 ##Items necsesarios
      @items_c = 0 ##Items conseguidos
      @items_img = Gosu::Image.new('media/images/levels/1/items/snowman.png') ##Icono items
    elsif @level == 2
      @items_n = 15
      @items_c = 0
      @items_img = Gosu::Image.new('media/images/levels/2/items/skeleton.png') ##Icono items
    elsif @level == 3
      @items_n = 20
      @items_c = 0
      @items_img = Gosu::Image.new('media/images/levels/3/items/mushroom_1.png') ##Icono items
    end
  end

  def draw
    @items_i = Gosu::Image.from_text("#{@items_c}/#{@items_n}",Utils::FONT_SIZE_SMALL,font:Utils.default_font) ##Info items conseguidos / items necsesarios
    @items_i.draw(1500,Y_POSITION,1) ##Dibujar Info items conseguidos / items necsesarios
    @items_img.draw(1700,Y_POSITION + 3,1,0.5,0.5) ##Dibujar icono items
  end

  def update
  end

  def update_score!
    @items_c += 1
  end
end
