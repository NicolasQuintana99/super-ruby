module Utils
  FONT_SIZE_BIG = 170
  FONT_SIZE_SMALL = 40
  TEXT_COLOR = Gosu::Color.new(0,0,225)
  TEXT_MENU_COLOR = Gosu::Color.new(231,1,24)
  TEXT_COLOR_LIGHT = Gosu::Color.new(45, 45, 45)
  SCREEN_WIDTH = 1920
  SCREEN_HEIGHT = 1080
  FLOOR_Y = 952
  OPTIONS_MARGIN_TOP = 400
  MARGIN_INTERNAL_Y = 140

  def self.default_font_size_big
    120
  end

  def self.default_font ## fuente por defecto
    '../media/fonts/grinched.ttf'
  end

  def self.center_x(object)
    (SCREEN_WIDTH / 2) - (object.width / 2)
  end

  def self.center_y(object)
    (SCREEN_HEIGHT / 2) - (object.height / 2)
  end

  def self.set_background
    @image_path = {
      snow: "snow",
      grass: "grass",
      desert: "desert"
    }
    @random_image = Random.rand(0..2)
    if @random_image == 0
      :snow
    elsif @random_image == 1
      :grass
    elsif @random_image == 2
      :desert
    end
  end

  def self.start_timmer
    Time.now
  end

  def self.compare_time?(actual_time, object_time, amount)
    @actual_time = actual_time.to_i
    if object_time + amount == @actual_time
      return true
    else
      return false
    end
  end

  def self.is_possible?(range, chance)
    @value = Random.rand(range)
    if @value <= chance
      return true
    else
      return false
    end
  end
end
