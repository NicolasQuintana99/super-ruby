#Nota para los profesores, las notas en las clases son guias para los desarolladores para mantener un control del codigo!

require 'gosu'
require 'csv'
require_relative 'Menu/main_menu'
require_relative 'Menu/menu_config'
require_relative 'Level/level_title'
require_relative 'Level/level'
require_relative 'Menu/confirmation'
require_relative 'Level/game_over'
require_relative 'utils'
class Game < Gosu::Window
  attr_accessor :actual_music, :song, :level, :timmer, :hero_run, :hero_jump, :hero_dead, :hero_idle, :current_hero_run, :current_hero_jump, :current_hero_dead, :current_hero_idle, :enemy_run, :enemy_jump, :enemy_dead, :enemy_idle, :enemy_walk, :current_enemy_run, :current_enemy_jump, :current_enemy_dead, :current_enemy_idle, :current_enemy_walk
  def initialize
    super(Utils::SCREEN_WIDTH, Utils::SCREEN_HEIGHT, fullscreen: false)
    self.caption = 'Super Ruby'
    @main_menu = MainMenu.new(self)
    @current_screen = @main_menu
    @actual_music = Gosu::Song.new('media/sounds/music/menu_music.ogg')
    @song = true
    @level = 1
    @charging_level = false
    @enemy_definitions = load_enemy_definitions
    @collect_definitions = load_collectable_definitions
    load_hero_animation
    load_enemy_animation
  end

  def draw
    @current_screen.draw
  end

  def update
    @actual_time = Time.now
    @current_screen.update
    unless @actual_music.playing?
      @actual_music.play if @song == true
    end
    if @charging_level == true #Controla el tiempo transcurrido para la ventana de carga
      if @timmer.to_i + 5 == @actual_time.to_i
        show_level!(@lives, @points)
        @charging_level = false
        @timmer = 0
      end
    end
    load_enemy_definitions
  end

  #interfaces del juego

  def return_game!#Retorna al juego tras la pausa, lo deja igual al estado previo a la misma
    @current_screen = @last_level
  end

  def show_game_over!(points)
    @current_screen = GameOver.new(self, points, @song)
  end

  def level_title!(lives, points)
    @lives = lives
    @points = points
    @current_screen = LevelTitle.new(self, @level)
    @timmer = Utils.start_timmer
    @charging_level = true
  end

  def show_level!(lives, points)#Reinicia el nivel o inicia uno nuevo
    @current_screen = Level.new(self, @enemy_definitions, @level, @collect_definitions = load_collectable_definitions, @points, @lives)
    @last_level = @current_screen
  end

  def show_confirmation!(background)#confirmacion para regresar al menu
    @current_screen = Confirmation.new(background, self)
  end

  def show_main_menu!
    @current_screen = @main_menu
    @actual_music = Gosu::Song.new('media/sounds/music/menu_music.ogg')
  end

  def show_menu_config!
    @current_screen = MenuConfig.new(self)
  end

  def button_down(id)
    @current_screen.button_down(id)
  end

  def load_enemy_definitions
    unless @level.nil?
      if @level == 1
        file_content = File.read('enemy1.csv')
      elsif @level == 2
        file_content = File.read('enemy2.csv')
      elsif @level == 3
        file_content = File.read('enemy3.csv')
      end
      rows = CSV.parse(file_content)
      rows.map do |row|
        {
          image_path: row[0],
          points: row[1].to_i,
          posibility: row[2].to_i
        }
      end
    end
  end

  def load_collectable_definitions
    if @level == 1
      file_content = File.read('collectables1.csv')
    elsif @level == 2
      file_content = File.read('collectables1.csv')
    elsif @level == 3
      file_content = File.read('collectables1.csv')
    end
    rows = CSV.parse(file_content)
    rows.map do |row|
      {
        image_path: row[0],
        points: row[1].to_i,
        posibility: row[2].to_i
      }
    end
  end

  def load_hero_animation
    @hero_run = []
    (1..10).each do |image|
      @source = "media/images/player/run_#{image}.png"
      @hero_run << Gosu::Image.new(@source, tileable:false)
    end
    @current_hero_run = 0

    @hero_idle = []
    (1..10).each do |image|
      @source = "media/images/player/idle_#{image}.png"
      @hero_idle << Gosu::Image.new(@source, tileable:false)
    end
    @current_hero_idle = 0

    @hero_dead = []
    (1..10).each do |image|
      @source = "media/images/player/dead_#{image}.png"
      @hero_dead << Gosu::Image.new(@source, tileable:false)
    end
    @current_hero_dead = 0

    @hero_jump = []
    (1..10).each do |image|
      @source = "media/images/player/jump_#{image}.png"
      @hero_jump << Gosu::Image.new(@source, tileable:false)
    end
    @current_hero_jump = 0
  end

  def load_enemy_animation
    info = @enemy_definitions.sample
    @image_path = info[:image_path]
    @enemy_run = []
    (1..10).each do |image|
      @source = "media/images/enemies/#{@image_path}/run_#{image}.png"
      @enemy_run << Gosu::Image.new(@source, tileable:false)
    end
    @current_enemy_run = 0

    @enemy_walk = []
    (1..10).each do |image|
      @source = "media/images/enemies/#{@image_path}/walk_#{image}.png"
      @enemy_walk << Gosu::Image.new(@source, tileable:false)
    end
    @current_enemy_walk = 0

    @enemy_idle = []
    (1..10).each do |image|
      @source = "media/images/enemies/#{@image_path}/idle_#{image}.png"
      @enemy_idle << Gosu::Image.new(@source, tileable:false)
    end
    @current_enemy_idle = 0

    @enemy_dead = []
    (1..10).each do |image|
      @source = "media/images/enemies/#{@image_path}/dead_#{image}.png"
      @enemy_dead << Gosu::Image.new(@source, tileable:false)
    end
    @current_enemy_dead = 0

    @enemy_jump = []
    (1..10).each do |image|
      @source = "media/images/enemies/#{@image_path}/jump_#{image}.png"
      @enemy_jump << Gosu::Image.new(@source, tileable:false)
    end
    @current_enemy_jump = 0
  end

end
