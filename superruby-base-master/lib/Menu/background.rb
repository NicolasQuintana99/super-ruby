class MenuBackground

  def initialize(path)
    @path = path
    @extra = Random.rand(0..1)
    @image = Gosu::Image.new("media/images/menu/#{@path}.png")
    if @extra == 1
      @hero = Gosu::Image.new("media/images/menu/player.png")
      if @path == "snow"
        @lantern = Gosu::Image.new("media/images/menu/lantern.png")
      elsif @path == "grass"
        @ninja = Gosu::Image.new("media/images/menu/ninja.png")
      elsif @path == "desert"
        @knight = Gosu::Image.new("media/images/menu/knight.png")
      end
    end
  end

  def draw
    @image.draw(0,0,0, 2, 2)
    @hero.draw(300, 400, 1)unless @hero.nil?
    @lantern.draw(700, 200)unless @lantern.nil?
    @ninja.draw(700, 200)unless @ninja.nil?
    @knight.draw(700, 200)unless @knight.nil?
  end
end
