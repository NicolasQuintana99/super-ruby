#Notas para el desarrollador
#el random para el background se define en set background y asi genera un background al azar para el menu!
#se pasa como parametro el background elegido para asi definirlo en la clase background!
#__________________________________________________________________________________________________________________
require_relative 'menu_option'
require_relative 'background'
# require_relative 'background'
class MainMenu
OPTIONS = {
  play: 0,
  config: 1,
  score: 2,
  exit: 3
}

  def initialize(window)
    @background = Utils.set_background
    @menu_background = MenuBackground.new(@background)
    @game = window
    @title = Gosu::Image.from_text("Menu principal", Utils::FONT_SIZE_BIG, font: Utils.default_font)
    @x = Utils.center_x(@title)
    @color = Utils::TEXT_MENU_COLOR
    @options = []
    ["Jugar", "Opciones", "Salir"].each_with_index do |text, index|
      option_y = Utils::OPTIONS_MARGIN_TOP + (index * Utils::MARGIN_INTERNAL_Y)
      @options << MenuOption.new(text, option_y)
    end
    @current_option = OPTIONS[:play]
    @menu_option_sound = Gosu::Sample.new('media/sounds/effects/menu_option.ogg')
  end

  def draw
    @menu_background.draw
    @title.draw(@x,0,2,1,1,@color)
    @options.each do |option|
      is_selected = option == @options[@current_option]
      option.draw(is_selected)
    end
  end

  def update
  end

  def button_down(id)
    case id
    when Gosu::KbEscape
      @game.close
    when Gosu::KbW
      move_dif_option!('up')
    when Gosu::KbS
      move_dif_option!('down')
    when Gosu::KbUp
      move_dif_option!('up')
    when Gosu::KbDown
      move_dif_option!('down')
    when Gosu::KbSpace
      select_option!
    when Gosu::KbEnter
      select_option!
    end
  end

  def move_dif_option!(sentido)
    if sentido == 'down'
      if @current_option == OPTIONS[:play]
        @current_option = OPTIONS[:config]
      elsif @current_option == OPTIONS[:config]
        @current_option =  OPTIONS[:exit]
      elsif @current_option == OPTIONS[:exit]
        @current_option =  OPTIONS[:play]
      end
     end
     if sentido == 'up'
       if @current_option == OPTIONS[:play]
         @current_option = OPTIONS[:exit]
       elsif @current_option == OPTIONS[:config]
         @current_option = OPTIONS[:play]
       elsif @current_option ==  OPTIONS[:exit]
         @current_option =  OPTIONS[:config]
       end
    end
    @menu_option_sound.play if @game.song == true
  end

  def select_option!
    @lives = 3
    @points = 0
    case @current_option
    when OPTIONS[:play]
      @game.level_title!(@lives, @points)
    when  OPTIONS[:config]
      @game.show_menu_config!
    when OPTIONS[:exit]
      @game.close
    end
  end
end
