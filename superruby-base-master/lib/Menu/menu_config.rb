require_relative 'menu_option'
require_relative 'background'
class MenuConfig
OPTIONS = {
  on: 0,
  off: 1
  }

  def initialize(window)
    @background = Utils.set_background
    @menu_background = MenuBackground.new(@background)
    @game = window
    @title = Gosu::Image.from_text("Musica", Utils::FONT_SIZE_BIG, font: Utils.default_font)
    @x = Utils.center_x(@title)
    @color = Utils::TEXT_MENU_COLOR
    @options = []
    ["Encendida", "Apagada"].each_with_index do |text, index|
      option_y = Utils::OPTIONS_MARGIN_TOP + (index * Utils::MARGIN_INTERNAL_Y)
      @options << MenuOption.new(text, option_y)
    end
    @current_option = OPTIONS[:on]
    @menu_option_sound = Gosu::Sample.new('media/sounds/effects/menu_option.ogg')
  end

  def draw
    @menu_background.draw

    @title.draw(@x,0,2,1,1,@color)
    @options.each do |option|
      is_selected = option == @options[@current_option]
      option.draw(is_selected)
    end
  end

  def update
  end

  def button_down(id)
    case id
    when Gosu::KbEscape
      @game.show_main_menu!
    when Gosu::KbW
      move_dif_option!
    when Gosu::KbS
      move_dif_option!
    when Gosu::KbUp
      move_dif_option!
    when Gosu::KbDown
      move_dif_option!
    when Gosu::KbSpace
      select_option!
    when Gosu::KbEnter
      select_option!
    end
  end

  def move_dif_option!
    if @current_option == OPTIONS[:on]
      @current_option = OPTIONS[:off]
    elsif @current_option == OPTIONS[:off]
      @current_option = OPTIONS[:on]
    end
    @menu_option_sound.play if @game.song == true
  end

  def select_option!
    case @current_option
    when OPTIONS[:on]
      @game.song = true
      @game.show_main_menu!
    when  OPTIONS[:off]
      @game.song = false
      @game.actual_music.stop
      @game.show_main_menu!
    end
  end
end
